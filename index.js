//alert("B249!");

class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4;
		return this;
	}
	willPass() {
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}

//class to represent a section of students
class Section {
	//every Section object will be instatiated with an empty array for its students
	constructor(name){
		this.name = name;
		this.students = [];
		this.honorStudents = 0;
		this.honorsPercentage = undefined;
		this.sectionAve = undefined;

	}

	//method for adding a student to this section
	//this will take in the same arguments needed to instantiate a Student object
	addStudent(name, email, grades) {
		//a Student object will be instantiated before being pushed to the students property
		this.students.push(new Student(name, email, grades));
		//return the Section object afterwards, allowing us to chain this method
		/////////
		//this.countHonorStudents();
		return this;
	}

	//method for computing how many students in the section are honor students
	countHonorStudents() {
			let count = 0;
			this.students.forEach(student => {

				if (student.computeAve().willPass().willPassWithHonors().passedWithHonors)
					{count ++}
			})
			this.honorStudents = count;
			return this;

		}

	/*
	Mini-Activity: 7:10PM

		Create a method that is a replica of computeHonorsPercentage()
			-create a property honorsPercentage with a value of undefined;
			-create computeHonorsPercentage() that will compute for the total number of honor students divided by total number of students multiplied by 100
			- the resulting value will be assigned to the property honorsPercentage
			-return the instance

	*/
	computeHonorsPercentage() {
		this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
		return this;
	}

	computeSectionAve() {
			let totalGrade= 0
			this.students.forEach(student => 
			{
				totalGrade += student.gradeAve
			})
			this.sectionAve = (totalGrade/ this.students.length);
			return this
		}

}

const section1A = new Section('section1A');
console.log(section1A);

/*
	Mini-Activity: 6:20PM

		Invoke the addStudent() method of the section1A while passing in the student's name, email and grades as arguments.

	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88]

	
*/

// section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88])
// section1A.addStudent("Jane", "jane@mail.com", [78, 82, 79, 85])
// section1A.addStudent("Joe", "joe@mail.com", [92, 89, 91, 96])
// section1A.addStudent("Jessie", "jessie@mail.com", [97, 95, 92, 93])

// console.log(section1A)

// console.log(section1A.students[0])

// Can we still invoke a particular student property and methods via dot notation? - YES: series of dot notation

// console.log(section1A.students[0].computeAve())

// Use the count honor students method
// console.log(section1A.countHonorStudents())



/* ACTIVITY! */

class Grade {
	constructor(level) {
		this.level = level;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade= undefined;
		this.batchMaxGrade = undefined
	}

	addSection(name){
		this.sections.push(new Section (sectionName));
		return this;
		}

	countStudents () {
		let count = 0;
		this.sections.forEach(section => this.totalStudents = this.totalStudents + section.students.length);
		return this;

	}

	countHonorStudents () {
		let honorCount = 0;
		this.sections.forEach(section => section.countHonorStudents())
		this.sections.forEach(section => honorCount += section.honorStudents)
			
		this.totalHonorStudents = honorCount;
		return this;
	}

	computeBatchAve() {
		let batchTotal = 0
		this.sections.forEach(section => section.computeSectionAve())
		this.sections.forEach(section => batchTotal += section.sectionAve)
		this.batchAveGrade = (batchTotal/this.sections.length)
		return this;
	}

}

	const grade1 = new Grade (1);


	grade1.addSection("section1A")
	grade1.addSection("section1B")
	grade1.addSection("section1C")
	grade1.addSection("section1D")
	console.log(grade1);

//save sections of this grade level as constants
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

//populate the sections with students
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

// console.log(`Count total students`)
// console.log(grade1.countStudents())

// console.log(`Count total honor students`)
// console.log(grade1.countHonorStudents())

// console.log(`Compute Batch Average`)
// console.log(grade1.computeBatchAve())


//grade1.section.find